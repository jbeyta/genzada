<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?>
	<footer role="contentinfo">
		<div class="row">
			<div class="s12 footer-info">
				<h5 class="footer-title">Contact Us</h5>

				<div class="contact-info">
					<?php echo do_shortcode('[contact_info phone="show"]'); ?>
					<div class="address-mother">
						<p class="site-name"><?php bloginfo(); ?></p>
						<?php echo do_shortcode('[contact_info address="show"]'); ?>
					</div>
					<?php echo do_shortcode('[contact_info email="show"]'); ?>
				</div>

				<div class="social-links">
					<?php echo do_shortcode('[contact_info social="show_all"]'); ?>
				</div>

				<?php
					$tou_page = cw_options_get_option( '_cwo_tou_page' );
					$tou_link = '';
					if($tou_page) {
						$tou_link = ' <a href="'.get_permalink($tou_page).'">'.get_the_title($tou_page).'</a>';
					}
				?>
				<p>&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>, All Rights Reserved.<?php echo $tou_link; ?></p>
			</div>
		</div>
	</footer>

	<!-- WP_FOOTER() -->
	<?php wp_footer(); ?>
</body>
</html>