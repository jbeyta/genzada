<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div id="main-content" role="main">
		<div class="row">
			<div class="s12">
				<?php
					while(have_posts()) {
						the_post();
						$hide_title = get_post_meta($post->ID, '_cwmb_hide_title', true);
						$titleclass = 'page-title';

						if($hide_title) {
							$titleclass = 'visually-hidden';
						}

						echo '<h2 class="'.$titleclass.'">'.get_the_title().'</h2>';
						the_content();
						// comments_template( '', true );
					}
				?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>