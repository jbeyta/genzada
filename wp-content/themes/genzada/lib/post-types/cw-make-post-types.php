<?php
class cwMakePostTypes {
	private $cw_all_cpt_caps = array();

	private function customCPTS() {
		$cw_post_types = array();

		$cpt_args = array(
			'post_type' => 'cw_post_type',
			'posts_per_page' => -1,
			'orderby' => 'title',
			'order' => 'ASC',
			'post_status' => 'publish'
		);

		$cpts = new WP_Query($cpt_args);

		if($cpts->have_posts()) {
			$predef_address_cpts = array();
			$pre_meta_boxes_from_cpts = array();
			$custom_meta_boxes_from_cpts = array();

			global $post;
			while($cpts->have_posts()) {
				$cpts->the_post();

				$predef_cpt_opts = array();

				$existing_pt = get_post_meta($post->ID, '_cwcpt_existing_pt', true);

				if(!empty($existing_pt)) {
					$pt_name = $existing_pt;
				} else {
					$pt_name = get_post_meta($post->ID, '_cwcpt_pt_name', true);

					$show_ui = true;
					$show_in_nav_menus = true;
					$show_in_menu = true;

					if(get_post_meta($post->ID, '_cwcpt_show_ui', true) == 'on') {
						$show_ui = false;
					}

					if(get_post_meta($post->ID, '_cwcpt_show_in_nav_menus', true) == 'on') {
						$show_in_nav_menus = false;
					}
					
					if(get_post_meta($post->ID, '_cwcpt_show_in_menu', true) == 'on') {
						$show_in_menu = false;
					}

					$predef_cpt_opts = array(
						'pt_name' => $pt_name,
						'admin_label' => get_post_meta($post->ID, '_cwcpt_admin_label', true),
						'singular' => get_post_meta($post->ID, '_cwcpt_singular', true),
						'plural' => get_post_meta($post->ID, '_cwcpt_plural', true),
						'menu_position' => get_post_meta($post->ID, '_cwcpt_menu_position', true),
						'hierarchical' => get_post_meta($post->ID, '_cwcpt_hierarchical', true),
						'has_archive' => get_post_meta($post->ID, '_cwcpt_has_archive', true),
						'supports' => get_post_meta($post->ID, '_cwcpt_supports', true),
						'categories' => get_post_meta($post->ID, '_cwcpt_categories', true),
						'tags' => get_post_meta($post->ID, '_cwcpt_tags', true),
						'icon' => get_post_meta($post->ID, '_cwcpt_icon', true),
						'capability_type' => get_post_meta($post->ID, '_cwcpt_page_post', true),
						'exclude_from_search' => get_post_meta($post->ID, '_cwcpt_exclude_from_search', true),
						'show_ui' => $show_ui,
						'show_in_nav_menus' => $show_in_nav_menus,
						'show_in_menu' => $show_in_menu,
					);

					array_push($cw_post_types, $predef_cpt_opts);
				}
				
				$pre_fields = get_post_meta($post->ID, '_cwcpt_pre_fields', true);				

				if(!empty($pre_fields)) {
					if( in_array('address', $pre_fields) ) {
						array_push($predef_address_cpts, $pt_name);
					}
				}

				$fields = get_post_meta($post->ID, '_cwcpt_cpt_fields', true);

				$custom_meta_boxes_from_cpts[$pt_name] = $fields;
			}

			$pre_meta_boxes_from_cpts['address'] = $predef_address_cpts;
		}

		wp_reset_query();

		return array(
			'cw_post_types' => $cw_post_types,
			'predef_mb' => $pre_meta_boxes_from_cpts,
			'custom_mb' => $custom_meta_boxes_from_cpts,
		);
	}

	private function preDefinedCPTS() {
		$cw_defaults = cw_cpt_options_get_option('_cwcpto_default_post_types');
		
		$cw_post_types = array();

		if(!empty($cw_defaults)) {
			foreach($cw_defaults as $cwd) {
				$plural = '';
				$singular = '';
				$supports = array();
				$icon = '';
				$archive = false;
				$hierarchical = true;
				$categories = 'no';
				$exclude_from_search = false;

				$predef_cpt_opts = array();

				// same opts for everyone
				$predef_cpt_opts['menu_position'] = '5';
				$predef_cpt_opts['menu_position'] = 5;
				$predef_cpt_opts['hierarchical'] = 1;
				$predef_cpt_opts['has_archive'] = 0;
				$predef_cpt_opts['tags'] = 0;
				$predef_cpt_opts['capability_type'] = 'post';
				$predef_cpt_opts['rewrite'] ='';
				$predef_cpt_opts['show_ui'] = true;
				$predef_cpt_opts['show_in_nav_menus'] = true;
				$predef_cpt_opts['show_in_menu'] = true;


				if($cwd == 'slides') {
					$predef_cpt_opts['singular'] = 'Slide';
					$predef_cpt_opts['plural'] = 'Slides';
					$predef_cpt_opts['hierarchical'] = true;
					$predef_cpt_opts['has_archive'] = false;
					$predef_cpt_opts['supports'] = array('title');
					$predef_cpt_opts['categories'] = false;
					$predef_cpt_opts['icon'] = 'dashicons-slides';
					$predef_cpt_opts['pt_name'] = $cwd;
					$predef_cpt_opts['admin_label'] = 'Slides';
					$predef_cpt_opts['exclude_from_search'] = true;
				}

				if($cwd == 'faqs') {
					$predef_cpt_opts['plural'] = 'FAQs';
					$predef_cpt_opts['singular'] = 'FAQ';
					$predef_cpt_opts['icon'] = 'dashicons-editor-help';
					$predef_cpt_opts['supports'] = array('title', 'editor', 'author', 'excerpt');
					$predef_cpt_opts['categories'] = 'yes';
					$predef_cpt_opts['hierarchical'] = true;
				}

				if($cwd == 'testimonials') {
					$predef_cpt_opts['plural'] = 'Testimonials';
					$predef_cpt_opts['singular'] = 'Testimonial';
					$predef_cpt_opts['icon'] = 'dashicons-admin-comments';
					$predef_cpt_opts['supports'] = array('title');
				}

				if($cwd == 'staff') {
					$predef_cpt_opts['plural'] = 'Staff';
					$predef_cpt_opts['singular'] = 'Listing';
					$predef_cpt_opts['icon'] = 'dashicons-groups';
					$predef_cpt_opts['supports'] = array('title', 'editor');
					$predef_cpt_opts['categories'] = 'yes';

				}

				if($cwd == 'services') {
					$predef_cpt_opts['plural'] = 'Services';
					$predef_cpt_opts['singular'] = 'Service';
					$predef_cpt_opts['icon'] = 'dashicons-hammer';
					$predef_cpt_opts['supports'] = array('title', 'editor', 'thumbnail');
					$predef_cpt_opts['categories'] = 'yes';
					$predef_cpt_opts['capability_type'] = 'page';
				}

				if($cwd == 'locations') {
					$predef_cpt_opts['plural'] = 'Locations';
					$predef_cpt_opts['singular'] = 'Location';
					$predef_cpt_opts['icon'] = 'dashicons-location-alt';
					$predef_cpt_opts['supports'] = array('title');
				}

				if($cwd == 'promos') {
					$predef_cpt_opts['plural'] = 'Promos';
					$predef_cpt_opts['singular'] = 'Promo';
					$predef_cpt_opts['icon'] = 'dashicons-format-image';
					$predef_cpt_opts['supports'] = array('title');
					$predef_cpt_opts['hierarchical'] = true;
					$predef_cpt_opts['categories'] = 'yes';
					$predef_cpt_opts['exclude_from_search'] = true;
				}

				if($cwd == 'galleries') {
					$predef_cpt_opts['plural'] = 'Galleries';
					$predef_cpt_opts['singular'] = 'Gallery';
					$predef_cpt_opts['icon'] = 'dashicons-images-alt2';
					$predef_cpt_opts['supports'] = array('title');
					$predef_cpt_opts['hierarchical'] = true;
					$predef_cpt_opts['categories'] = 'yes';
				}

				if($cwd == 'classes') {
					$predef_cpt_opts['plural'] = 'Classes';
					$predef_cpt_opts['singular'] = 'Class';
					$predef_cpt_opts['icon'] = 'dashicons-schedule';
					$predef_cpt_opts['supports'] = array('title', 'editor');
					$predef_cpt_opts['hierarchical'] = true;
					$predef_cpt_opts['categories'] = 'yes';
				}

				if($cwd == 'alerts') {
					$predef_cpt_opts['plural'] = 'Alerts';
					$predef_cpt_opts['singular'] = 'Alert';
					$predef_cpt_opts['icon'] = 'dashicons-warning';
					$predef_cpt_opts['supports'] = array('title', 'editor');
					$predef_cpt_opts['hierarchical'] = true;
					$predef_cpt_opts['exclude_from_search'] = true;
				}

				array_push($cw_post_types, $predef_cpt_opts);
			}
		}

		return $cw_post_types;
	}

	public function registerCPTS() {
		$cw_post_types = array();
		
		$predef = $this->preDefinedCPTS();
		if(!empty($predef)) {
			foreach ($predef as $predefcpt) {
				array_push($cw_post_types, $predefcpt);
			}
		}
		
		$custom = $this->customCPTS();
		if(!empty($custom['cw_post_types'])) {
			foreach ($custom['cw_post_types'] as $customcpt) {
				array_push($cw_post_types, $customcpt);
			}
		}		

		if(!empty($cw_post_types)) {
			$cw_post_types = array_values($cw_post_types);

			foreach($cw_post_types as $cw_post_type) {
				$title = $cw_post_type['admin_label'];

				$s_name = $cw_post_type['singular'];
				$lower_s_name =  str_replace(' ', '-', strtolower($cw_post_type['singular']));
				$lower_s_key_name =  str_replace(' ', '_', strtolower($cw_post_type['singular']));

				$pl_name = $cw_post_type['plural'];
				$lower_pl_name = str_replace(' ', '-', strtolower($cw_post_type['plural']));
				$lower_pl_key_name = str_replace(' ', '_', strtolower($cw_post_type['plural']));

				$post_type_name = $cw_post_type['pt_name'];
				if(empty($cw_post_type['pt_name'])) {
					$post_type_name = $lower_pl_name;
				}

				if($lower_pl_key_name != $post_type_name) {
					$lower_s_key_name = $post_type_name.'_'.$lower_s_key_name;
					$lower_pl_key_name = $post_type_name.'_'.$lower_pl_key_name;
				}

				$post_type_name_title = ucfirst($post_type_name);

				$supports = $cw_post_type['supports'];
				$menu_pos = $cw_post_type['menu_position'];
				$hierarchical = $cw_post_type['hierarchical'];
				$has_archive = $cw_post_type['has_archive'];
				$rewrite = $cw_post_type['rewrite'];
				$exclude_from_search = $cw_post_type['exclude_from_search'];

				$icon = $cw_post_type['icon'];

				$uc_pl_name = ucfirst($pl_name);

				$admin_label = '';
				if(!empty($title)) {
					$admin_label = $title;
				} else {
					$admin_label = $pl_name;
				}

				$capability_type = $cw_post_type['capability_type'];
				if(empty($capability_type)) {
					$capability_type = "post";
				}

				if( !empty($hierarchical) ) {
					$hierarchical = true;
					$capability_type = "page";
					array_push($supports, 'page-attributes');
				} else {
					$hierarchical = false;
				}

				if(!empty($has_archive)) {
					$has_archive = true;
				} else {
					$has_archive =  false;
				}

				if(!empty($rewrite)) {
					$rewrite = urlencode($rewrite);
				} else {
					$rewrite = urlencode($post_type_name);
				}

				if(
					$post_type_name == 'slides'
					|| $post_type_name == 'cw_post_type'
					|| $post_type_name == 'alerts'
					|| $post_type_name == 'promos'
				) {
					$exclude_from_search = true;
				}

				$field_args = array(
					'labels' => array(
						'name' => $admin_label,
						'singular_name' => $pl_name,
						'add_new' => 'Add New '.$s_name,
						'add_new_item' => 'Add New '.$s_name,
						'edit_item' => 'Edit '.$s_name,
						'new_item' => 'Add New '.$s_name,
						'view_item' => 'View '.$s_name,
						'search_items' => 'Search '.$pl_name,
						'not_found' => 'No '.$lower_pl_name.' found',
						'not_found_in_trash' => 'No '.$lower_pl_name.' found in trash'
					),
					'public' => true,
					'publicly_queryable' => true,
					'show_ui' => $cw_post_type['show_ui'],
					'show_in_menu' => $cw_post_type['show_in_menu'],
					'show_in_nav_menus' => $cw_post_type['show_in_nav_menus'],
					'exclude_from_search' => $exclude_from_search,
					'capability_type' => $capability_type,
					'has_archive' => $has_archive,
					'slug' => strtolower($post_type_name),
					'hierarchical' => $hierarchical,
					'query_var' => true,
					'rewrite' => array( 'slug' => $rewrite ),
					'menu_position' => null,
					'supports' => $supports,
					'menu_icon' => $icon,
					'show_in_rest' => true,
					'rest_base' => $post_type_name,
					'rest_controller_class' => 'WP_REST_Posts_Controller',
					// 'capability_type' => array($lower_s_name, $lower_pl_name),
					'map_meta_cap'=> true,
				);

				$caps_for_roles = array(
					'edit_'.$post_type_name,
					'edit_'.$post_type_name,
					'edit_other_'.$post_type_name,
					'publish_'.$post_type_name,
					'read_'.$post_type_name,
					'read_private_'.$post_type_name,
					'delete_'.$post_type_name,
				);

				foreach ($caps_for_roles as $cfr) {
					array_push($this->cw_all_cpt_caps, $cfr);
				}

				$rgstrd = register_post_type($post_type_name, $field_args);

				if($cw_post_type['categories'] == 'yes') {
					$cat_s = $post_type_name_title.' Category';
					$cat_pl = $post_type_name_title.' Categories';

					if($s_name == 'Promo') {
						$cat_s = 'Position';
						$cat_pl = 'Positions';
					}

					$field_args = array(
						'labels' => array(
							'name'			  => _x( $cat_pl, 'taxonomy general name' ),
							'singular_name'	 => _x( $cat_s, 'taxonomy singular name' ),
							'search_items'	  => __( 'Search '.$cat_pl ),
							'all_items'		 => __( 'All '.$cat_pl ),
							'parent_item'	   => __( 'Parent '.$cat_s ),
							'parent_item_colon' => __( 'Parent '.$cat_s.':' ),
							'edit_item'		 => __( 'Edit '.$cat_s ),
							'update_item'	   => __( 'Update '.$cat_s ),
							'add_new_item'	  => __( 'Add New '.$cat_s ),
							'new_item_name'	 => __( 'New '.$cat_s ),
							'menu_name'		 => __( $cat_pl )
						),
						'rewrite' => array(
							'slug' => $post_type_name.'_category',
							'with_front' => false
						),
						'hierarchical' => true,
						'show_ui' => true,
						'show_admin_column' => true,
						'show_in_rest' => true,
						'capabilities' => array (
							'manage_terms' => 'manage_options', //by default only admin
							'edit_terms' => 'manage_options',
							'delete_terms' => 'manage_options',
							'assign_terms' => 'edit_'.$post_type_name,  // can edit cpt-type i.e. custom role
						),
					);
					register_taxonomy( $post_type_name.'_categories', $post_type_name, $field_args );
				} // end if categories

				if($cw_post_type['tags'] == 'yes') {
					$tags_args = array(
						'labels' => array(
							'name' => _x( $post_type_name_title.' Tags', 'taxonomy general name' ),
							'singular_name' => _x( $post_type_name_title.' Tag', 'taxonomy singular name' ),
							'search_items' =>  __( 'Search Tags' ),
							'popular_items' => __( 'Popular Tags' ),
							'all_items' => __( 'All Tags' ),
							'parent_item' => null,
							'parent_item_colon' => null,
							'edit_item' => __( 'Edit Tag' ),
							'update_item' => __( 'Update Tag' ),
							'add_new_item' => __( 'Add New Tag' ),
							'new_item_name' => __( 'New Tag Name' ),
							'separate_items_with_commas' => __( 'Separate tags with commas' ),
							'add_or_remove_items' => __( 'Add or remove tags' ),
							'choose_from_most_used' => __( 'Choose from the most used tags' ),
							'menu_name' => __( 'Tags' ),
						),
						'hierarchical' => false,
						'show_ui' => true,
						'query_var' => true,
						'rewrite' => array('slug' => 'tag', 'with_front' => false),
						'show_in_rest' => true,
						'capabilities' => array (
							'manage_terms' => 'manage_options', //by default only admin
							'edit_terms' => 'manage_options',
							'delete_terms' => 'manage_options',
							'assign_terms' => 'edit_'.$post_type_name,  // can edit cpt-type i.e. custom role
						),
					);
					register_taxonomy($post_type_name.'_tags',$post_type_name, $tags_args);
				} // end if tags
			} // end main foreach post type
		}

		$this->cw_add_theme_caps();

		return array(
			'predef' => $custom['predef_mb'],
			'custom' => $custom['custom_mb'],
		);
	}

	// add caps to roles
	private function cw_add_theme_caps() {
		$roles = array('administrator');

		foreach ($roles as $role) {
			$admins = get_role( $role );
			
			$admins->add_cap( 'read' ); 

			foreach ($this->cw_all_cpt_caps as $cap) {
				$admins->add_cap( $cap ); 
			}
		}
	}
}

function cw_cpt_custom_metaboxes( array $meta_boxes ) {
	$cwMakePostTypes = new cwMakePostTypes();
	$meta_box_data = $cwMakePostTypes->registerCPTS();

	$custom_meta_boxes_from_cpts = $meta_box_data['custom'];
	$pre_meta_boxes_from_cpts = $meta_box_data['predef'];

	$prefix = '_cwmb_'; // Prefix for all fields

	global $cw_states;

	if(!empty($custom_meta_boxes_from_cpts)) {
		foreach ($custom_meta_boxes_from_cpts as $post_type => $fields) {
			$field_id = $post_type.'_meta';

			if(!empty($fields)) {
				$$field_id = new_cmb2_box( array(
					'id' => $prefix.$post_type.'_meta_box',
					'title' => 'Options',
					'object_types'  => array( $post_type ), // Post type
					'context' => 'normal',
					'priority' => 'default',
					'show_names' => true, // Show field names on the left
				) );

				foreach ($fields as $f_data) {
					$field_options = array();
					
					$field_options['id'] = $prefix.$f_data['field_id'];
					$field_options['name'] = $f_data['field_name'];
					$field_options['type'] = $f_data['field_type'];
					
					if(!empty($f_data['field_desc'])) {
						$field_options['desc'] = $f_data['field_desc'];
					}

					if(!empty($f_data['field_options'])) {
						$options = array();
						$xplode = explode(PHP_EOL, $f_data['field_options']);
						foreach ($xplode as $opt) {
							$parts = explode('|', $opt);
							if(count($parts) <= 1) {
								$slug = strtolower( str_replace( ' ', '_', preg_replace( "/[^A-Za-z0-9 ]/", '', $opt ) ) );
								$options[$slug] = $opt;
							} else {
								$options[$parts[0]] = $parts[1];
							}
						}
						$field_options['options'] = $options;
					}

					if(!empty($f_data['field_atts'])) {
						$attributes = array();
						$xplode = explode(PHP_EOL, $f_data['field_atts']);
						foreach ($xplode as $opt) {
							$plode_opt = explode('|', $opt);
							$attributes[$plode_opt[0]] = $plode_opt[1];
						}
						$field_options['attributes'] = $attributes;
					}

					if(!empty($f_data['field_query_args'])) {
						$mimes = explode(PHP_EOL, $f_data['field_query_args']);
						$field_options['query_args']['type'] = $mimes;
					}

					$$field_id->add_field( $field_options );
				}
			}
		}
	}

	// address
	if(!empty($pre_meta_boxes_from_cpts['address'])) {
		$address = new_cmb2_box( array( 'id' => $prefix.'address', 'title' => 'Address', 'object_types' => $pre_meta_boxes_from_cpts['address'], 'context' => 'normal', 'priority' => 'default', 'show_names' => true, ) );
		$address->add_field( array( 'name' => 'Contact Person', 'id' => $prefix.'loc_poc', 'type' => 'text' ) );
		$address->add_field( array( 'name' => 'Logo/Photo', 'id' => $prefix.'loc_photo', 'type' => 'file', 'query_args' => array( 'type' => array( 'image/gif', 'image/jpeg', 'image/png', ), ) ) );
		$address->add_field( array( 'name' => 'Address 1', 'id' => $prefix.'loc_address1', 'type' => 'text' ) );
		$address->add_field( array( 'name' => 'Address 2', 'id' => $prefix.'loc_address2', 'type' => 'text' ) );
		$address->add_field( array( 'name' => 'City', 'id' => $prefix.'loc_city', 'type' => 'text' ) );
		$address->add_field( array( 'name' => 'State', 'id' => $prefix.'loc_state', 'type' => 'select', 'options' => $cw_states ) );
		$address->add_field( array( 'name' => 'Zip', 'id' => $prefix.'loc_zip', 'type' => 'text' ) );
		$address->add_field( array( 'name' => 'Phone', 'id' => $prefix.'loc_phone', 'type' => 'text' ) );
		$address->add_field( array( 'name' => 'Phone 2', 'id' => $prefix.'loc_phone2', 'type' => 'text' ) );
		$address->add_field( array( 'name' => 'Fax', 'id' => $prefix.'loc_fax', 'type' => 'text' ) );
		$address->add_field( array( 'name' => 'Email', 'id' => $prefix.'loc_email', 'type' => 'text' ) );
		$address->add_field( array( 'name' => 'Website', 'id' => $prefix.'loc_website', 'type' => 'text_url' ) );
		$address->add_field( array( 'name' => 'Hours', 'id' => $prefix.'loc_hours', 'type' => 'textarea' ) );
		$address->add_field( array( 'name' => 'Info', 'id' => $prefix.'loc_info', 'type' => 'textarea' ) );
		$address->add_field( array( 'name' => 'Lat', 'desc' => 'In the case that Google maps show the inccorrect location, use lat & lon', 'id' => $prefix.'loc_lat', 'type' => 'text' ) );
		$address->add_field( array( 'name' => 'Lon', 'desc' => 'In the case that Google maps show the inccorrect location, use lat & lon', 'id' => $prefix.'loc_lon', 'type' => 'text' ) );
	}

	return $meta_boxes;
}
add_filter( 'cmb2_meta_boxes', 'cw_cpt_custom_metaboxes' );