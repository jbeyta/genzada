<?php
// echo out promo from a position, menu_order by default
function cw_get_promo($position, $orderby = 'menu_order', $order = 'ASC') {
	if(empty($position)) {
		return;
	}

	global $post;

	$pargs = array(
		'post_type' => 'promos',
		'posts_per_page' => 1,
		'orderby' => $orderby,
		'order' => $order,
		'tax_query' => array(
			array (
				'taxonomy' => 'promos_categories',
				'field' => 'slug',
				'terms' => $position
			)
		)
	);

	$promos = new WP_Query($pargs);
	if($promos->have_posts()){
		while($promos->have_posts()) {
			$promos->the_post();

			$image_id = get_post_meta($post->ID, '_cwmb_promo_image_id', true);
			$url = get_post_meta($post->ID, '_cwmb_promo_link', true);

			if(!empty($image_id))	{
				$custom = array(
					'large' => array(
						'w' => 1280,
						'h' => 800,
						'crop' => true,
						'single' => true,
						'upscale' => true,
					),
					'medium' => array(
						'w' => 640,
						'h' => 400,
						'crop' => true,
						'single' => true,
						'upscale' => true,
					),
					'small' => array(
						'w' => 320,
						'h' => 200,
						'crop' => true,
						'single' => true,
						'upscale' => true,
					)
				);

				echo '<div class="promo">';
					if(!empty($url)) { echo '<a href="'.$url.'">'; }
						cw_img($image_id, 'full', $custom);
					if(!empty($url)) { echo '</a>'; }
				echo '</div>';
			}
		}
	}

	wp_reset_query();
}