<?php
// staff
function cw_get_staff_sc( $atts, $content = null ) {
	extract(shortcode_atts(array(
		'number' => -1,
		'cat' => '',
		'showtitles' => true
	), $atts));

	ob_start();

		$args = array(
			'post_type' => 'staff',
			'posts_per_page' =>  $number,
			'orderby' => 'menu_order',
			'order' => 'ASC',
		);

		if($cat) {
			$args['tax_query'] = array(
				array(
					'taxonomy' => 'staff_categories',
					'field' => 'slug',
					'terms' => $cat
				)
			);
		}

		$posts = new WP_Query($args);

		if(!$number || !$number == -1) {
			$number = $posts->found_posts;
		}

		if($posts->have_posts()) {
            global $post;
            echo '<div class="staff-from-sc number-'.$number.' cat-'.$cat.'">';
                while($posts->have_posts()) {
					$posts->the_post();
					if($showtitles) {
						get_template_part('template-parts/content', 'staff');
					} else {
						get_template_part('template-parts/content', 'staffnotitle');
					}
                }
            echo '</div>';
		}
		wp_reset_query();

	$temp = ob_get_contents();
	ob_end_clean();

	return $temp;
}
add_shortcode( 'staff', 'cw_get_staff_sc' );