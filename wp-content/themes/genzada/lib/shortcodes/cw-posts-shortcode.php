<?php
// latestposts
function cw_get_latestposts_sc( $atts, $content = null ) {
	extract(shortcode_atts(array(
		'number' => -1,
		'grid' => false
	), $atts));

	ob_start();

		$args = array(
			'post_type' => 'post',
			'posts_per_page' =>  $number,
			'orderby' => 'date',
			'order' => 'DESC',
		);
		$posts = new WP_Query($args);
		if($posts->have_posts()) {
			global $post;
			if($grid) { echo '<div class="news-grid">'; }
				while($posts->have_posts()) {
					$posts->the_post();
					$weblink = get_post_meta($post->ID, '_cwmb_weblink', true);

					if($grid) {
						echo '<div class="news-grid-item">';
							if(has_post_thumbnail()) {
								echo '<div class="img-mother">';
									the_post_thumbnail();
								echo '</div>';
							} else {
								// echo '<div class="img-mother noimg"></div>';
							}

							echo '<div class="info"><div class="inner">';
								//categories
								$terms = wp_get_object_terms( $post->ID, 'category' );
								if(!empty($terms)) {
									echo '<p class="categories">';
									foreach ($terms as $term) {
										echo '<a class="term-link" href="'.get_term_link($term->term_id, 'category').'">'.$term->name.'</a>';
									}
									echo '</p>';
								}

								echo '<h3 class="article-title green-text">'.get_the_title().'</h3>';

								echo '<p class="date">'.get_the_date().'</p>';

								echo '<p class="excerpt">';
									echo cw_excerpt(get_the_excerpt(), 25);
								echo '&hellip;</p>';

								if($weblink) {
									echo '<a href="'.$weblink.'" class="button">Read More</a>';
								}
								
							echo '</div></div>';
						echo '</div>';
					} else {
						get_template_part('template-parts/content', 'post');
					}
				} // end while
			if($grid) { echo '</div>'; } // end news grid
		} // end if
		wp_reset_query();

	$temp = ob_get_contents();
	ob_end_clean();

	return $temp;
}
add_shortcode( 'latest-posts', 'cw_get_latestposts_sc' );