<?php
// research
function cw_get_research_sc( $atts, $content = null ) {
	extract(shortcode_atts(array(
		'number' => -1
	), $atts));

	ob_start();

		$args = array(
			'post_type' => 'research',
			'posts_per_page' =>  $number,
			'orderby' => 'date',
			'order' => 'DESC',
		);
		$posts = new WP_Query($args);
		if($posts->have_posts()) {
			global $post;
			while($posts->have_posts()) {
				$posts->the_post();
				get_template_part('template-parts/content', 'research');
			}
		}
		wp_reset_query();

	$temp = ob_get_contents();
	ob_end_clean();

	return $temp;
}
add_shortcode( 'latest-research', 'cw_get_research_sc' );