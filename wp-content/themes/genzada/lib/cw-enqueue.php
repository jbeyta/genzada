<?php
/**
 * Enqueue front end scripts for CW theme.
 */
add_action("wp_enqueue_scripts", "cw_enqueue_frontend", 11);
function cw_enqueue_frontend() {
	wp_enqueue_script('modernizr', get_template_directory_uri().'/src/js/vendor/modernizr.js', array('jquery'), '2.6.2', false);

	// Enqueue the threaded comments reply scipt when necessary.
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// wp_enqueue_script('fontawesome', get_template_directory_uri().'/src/js/vendor/fontawesome-all.min.js', array(), '2.5.9', false);

	$isdev = cw_is_dev();

	// theme styles
	$js_file = '/dist/main.js';
	$css_file = '/dist/css/main.css';

	$js_mtime = filemtime(get_stylesheet_directory().$js_file);
	wp_enqueue_script('cw_js', get_template_directory_uri().$js_file, array('jquery'), $js_mtime, true);

	$mtime = filemtime(get_stylesheet_directory().$css_file);
	wp_register_style( 'cw-stylesheet', get_bloginfo('template_url').$css_file, array(), $mtime, 'all');

	// enqueue the stylesheet
	wp_enqueue_style( 'cw-stylesheet' );

	// wp_enqueue_script('gsap', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js', array('jquery'), '1.19.1', false);

	// map geo location stuff
	// $map_options = array();

	// $hide_controls = cw_contact_get_option( '_cwc_hide_controls' );
	// $allow_scroll_zoom = cw_contact_get_option( '_cwc_allow_scroll_zoom' );
	// $zoom_level = cw_contact_get_option( '_cwc_zoom_level' );
	// $saturation = intval( cw_contact_get_option( '_cwc_saturation' ) );
	// $simplified = cw_contact_get_option( '_cwc_simplified' );
	// $hue_hex = cw_contact_get_option( '_cwc_hue_hex' );

	// $controls = false;
	// if(!empty($hide_controls) && $hide_controls == 'on') {
	// 	$controls = true;
	// } else {
	// 	$controls = false;
	// }

	// $scroll_zoom = false;
	// if(!empty($allow_scroll_zoom) && $allow_scroll_zoom == 'on') {
	// 	$scroll_zoom = true;
	// } else {
	// 	$scroll_zoom = false;
	// }

	// $zoom = 14;
	// if(!empty($zoom_level)) {
	// 	$zoom = $zoom_level;
	// }

	// $satch = 0;
	// if($saturation === 0) {
	// 	$satch = -100;
	// }
	// if(!empty($saturation)) {
	// 	$amount = $saturation - 100;
	// 	$satch = $amount;
	// }

	// $visibility = 'on';
	// if(!empty($simplified) && $simplified == 'on') {
	// 	$visibility = 'simplified';
	// }

	// $hue = '';
	// if(!empty($hue_hex)) {
	// 	$hue = $hue_hex;
	// }

	// $map_options['maps_api_key'] = CW_MAPS_API_KEY;
	// $map_options['controls'] = $controls;
	// $map_options['scroll_zoom'] = $scroll_zoom;
	// $map_options['zoom'] = $zoom;
	// $map_options['satch'] = $satch;
	// $map_options['visibility'] = $visibility;
	// $map_options['hue'] = $hue;

	// wp_localize_script( 'cw_js', 'map_options', $map_options );

	// add search route to global js
	wp_localize_script('cw_js', 'search_route', get_bloginfo('url').'/wp-json/cw-search/v2/search/');

	// slideshow options
	// $mode = cw_slideshow_options_get_option('_cwso_mode');
	// $controls = cw_slideshow_options_get_option('_cwso_controls');
	// $pager = cw_slideshow_options_get_option('_cwso_pager');
	// $speed = cw_slideshow_options_get_option('_cwso_speed');
	// $pause = cw_slideshow_options_get_option('_cwso_pause');

	// $fade = 0;
	// $vertical = 0;

	// if(!empty($mode) && $mode == 'fade') {
	// 	$fade = 1;
	// }

	// if(!empty($mode) && $mode == 'vertical') {
	// 	$vertical = 1;
	// }

	// if(empty($controls)) {
	// 	$controls = true;
	// }

	// if(empty($pager)) {
	// 	$pager = false;
	// }

	// if(empty($speed)) {
	// 	$speed = '500';
	// } else {
	// 	$speed = $speed*1000;
	// }

	// if(empty($pause)) {
	// 	$pause = '5000';
	// } else {
	// 	$pause = $pause*1000;
	// }

	// $slideshow_options = array(
	// 	'fade' => $fade,
	// 	'vertical' => $vertical,
	// 	'controls' => $controls,
	// 	'pager' => $pager,
	// 	'speed' => $speed,
	// 	'pause' => $pause,
	// );

	// wp_localize_script('cw_js', 'slideshow_options', $slideshow_options);

	// localize endpoints
	$endpoints = array(
		'slides' => get_bloginfo('url').'/wp-json/cw/v2/slides',
		'images' => get_bloginfo('url').'/wp-json/cw/v2/img',
		'contentbyid' => get_bloginfo('url').'/wp-json/wp/v2/posts/'
	);

	wp_localize_script('cw_js', 'endpoints', $endpoints);
	wp_localize_script('cw_js', 'siteurl', get_bloginfo('url'));

	// localize post data
	// global $post;
	// $post_data = array();

	// $post_data['id'] = $post->ID;
	// $post_data['template'] = get_page_template_slug($post->ID);
	// $post_data['type'] = get_post_type($post->ID);
	// wp_localize_script('cw_js', 'post_data', $post_data);=
}

function cw_enqueue_admin() {
	$css_file = '/dist/css/admin.css';

	wp_register_style( 'cw_admin_css', get_template_directory_uri() . $css_file, false, '1.0.0' );
	wp_enqueue_style( 'cw_admin_css' );

	// enqueue admin js, including blocks
	wp_enqueue_script('cw_admin_js', get_template_directory_uri().'/dist/admin.js', array('wp-blocks', 'wp-element', 'wp-i18n'), '1.0.1', true);

	// $cpts = get_post_types();
	// ksort($cpts);

	// unset($cpts['post']);
	// unset($cpts['page']);
	// unset($cpts['attachment']);
	// unset($cpts['revision']);
	// unset($cpts['nav_menu_item']);
	// unset($cpts['custom_css']);
	// unset($cpts['customize_changeset']);
	// unset($cpts['oembed_cache']);
	// unset($cpts['user_request']);
	// unset($cpts['wp_block']);
	// unset($cpts['cw_post_type']);
	// unset($cpts['alerts']);
	// unset($cpts['classes']);

	// $cw_cpts = array(array('id' => '', 'name' => 'Select a Type'));

	// foreach ($cpts as $cpt) {
	// 	$cpt_data = array();
	// 	$cpt_data['id'] = $cpt;
	// 	$cpt_data['name'] = ucfirst($cpt);
	// 	array_push($cw_cpts, $cpt_data);
	// }

	// echo_pre($cw_cpts);

	$cw_cpts = array(
		array(
			'id' => '',
			'name' => 'Select a Type'
		),
		array(
			'id' => 'faqs',
			'name' => 'FAQs'
		),
		array(
			'id' => 'testimonials',
			'name' => 'Testimonials'
		),
		array(
			'id' => 'staff',
			'name' => 'Staff'
		),
	);

	wp_localize_script('cw_admin_js', 'cw_cpts', $cw_cpts);
	wp_localize_script('cw_admin_js', 'siteurl', get_bloginfo('url'));

	$endpoints = array(
		'featuredget' => get_bloginfo('url').'/wp-json/cw/v2/featured-get',
		'featuredsave' => get_bloginfo('url').'/wp-json/cw/v2/featured-save',
	);

	wp_localize_script('cw_admin_js', 'endpoints', $endpoints);
}
add_action( 'admin_enqueue_scripts', 'cw_enqueue_admin' );