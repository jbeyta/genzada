<?php
// endpoints
get_template_part('lib/endpoints/cw-images');
get_template_part('lib/endpoints/cw-slides');
get_template_part('lib/endpoints/cw-featured');

// expose meta or whatever in endpoint
// add_action( 'rest_api_init', 'cw_restapi_field_meta' );
// function cw_restapi_field_meta() {
// 	register_rest_field( 'galleries',
// 		'post_meta',
// 		array(
// 			'get_callback' => 'cw_gallery_meta',
// 			'update_callback' => null,
// 			'schema' => null,
// 		)
// 	);
// }

// function cw_gallery_meta( $object, $field_name, $request ) {
// 	return get_post_meta($object['id'], '_cwmb_gallery_desc', true);
// }

