<?php
// image
function cw_ajax_get_img()
{
	register_rest_route('cw/v2', '/img/', array(
		'methods' => 'GET',
		'callback' => 'cw_get_img'
	));
}
add_action('rest_api_init', 'cw_ajax_get_img');

function cw_get_img(WP_REST_Request $request)
{
	$img_html = '';

	if (isset($_GET['img_id']) && !empty($_GET['img_id'])) {
		$cutsom = array();

		$crop = false;

		if (isset($_GET['crop'])) {
			$crop = $_GET['crop'];
		}

		if (!empty($_GET['imgW']) || !empty($_GET['imgH'])) {
			$imgW = $_GET['imgW'];
			$imgH = $_GET['imgH'];

			if (!$imgW) { $imgW = NULL; }
			if (!$imgH) { $imgH = NULL; }

			$custom['cwimg_large'] = array(
				'w' => $imgW,
				'h' => $imgH,
				'crop' => $crop,
			);
		}

		if (!empty($_GET['imgWMed']) || !empty($_GET['imgHMed'])) {
			$imgWMed = $_GET['imgWMed'];
			$imgHMed = $_GET['imgHMed'];

			if (!$imgWMed) {
				$imgWMed = NULL;
			}
			if (!$imgHMed) {
				$imgHMed = NULL;
			}

			$custom['cwimg_med'] = array(
				'w' => $imgWMed,
				'h' => $imgHMed,
				'crop' => $crop,
			);
		}

		if (!empty($_GET['imgWSmall']) || !empty($_GET['imgHSmall'])) {
			$imgWSmall = $_GET['imgWSmall'];
			$imgHSmall = $_GET['imgHSmall'];

			if (!$imgWSmall) {
				$imgWSmall = NULL;
			}
			if (!$imgHSmall) {
				$imgHSmall = NULL;
			}

			$custom['cwimg_small'] = array(
				'w' => $imgWSmall,
				'h' => $imgHSmall,
				'crop' => $crop,
			);
		}

		$img_html = get_cw_img($_GET['img_id'], 'cwimg', $custom, '', true, true);
	}
	return $img_html;
	exit;
}
