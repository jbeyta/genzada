<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?><!DOCTYPE html>

<!--[if lt IE 7]> <html style="margin-top: 0!important;" class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html style="margin-top: 0!important;" class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html style="margin-top: 0!important;" class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html style="margin-top: 0!important;" class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

<head >
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="author" href="<?php echo get_template_directory_uri(); ?>/humans.txt">
	<link rel="dns-prefetch" href="//ajax.googleapis.com">
	<link rel="stylesheet" href="https://use.typekit.net/xxa0nlq.css">

	<!-- WP_HEAD() -->
	<?php wp_head(); ?>

	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/ie.css">
	<![endif]-->

	<?php
		$tracking = cw_options_get_option( '_cwo_tracking_code' );
		if(!empty($tracking)) {
			echo $tracking;
		}
	?>
</head>

<body <?php body_class(); ?>>
	<div>
		<a class="screen-reader-text" href="#main-content">skip navigation</a>
	</div>
	
	<?php
		$ga_code = cw_options_get_option('_cwo_ga');
		if( !empty($ga_code) ) {

		// only put the tracking on code when the site is not on a .dev
		// $extension = pathinfo($_SERVER['SERVER_NAME'], PATHINFO_EXTENSION);
		?>
			<script type="text/javascript">
				(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

				ga('create', '<?php echo $ga_code; ?>', 'auto');
				ga('send', 'pageview');
			</script>
	<?php } ?>

	<?php get_template_part('template-parts/content', 'browse-happy'); ?>

	<header role="banner">
		<div class="superheader">
			<div class="row">
				<div class="s12 inner">
					<?php echo do_shortcode('[contact_info social="show_all"]'); ?>
				</div>
			</div>
		</div>

		<div class="cw-nav-cont">
			<div class="row ai-center">
				<div class="s4">
					<?php cw_logo(true, true); ?>
				</div>

				<nav class="cw-nav s8" role="navigation">
					<span class="menu-toggle" data-menu="cw-nav-ul">
						<span class="open">
							<?php echo cw_get_icon_svg('menu'); ?>
						</span>

						<span class="close">
							<?php echo cw_get_icon_svg('close'); ?>
						</span>
					</span>
					
					<?php
						wp_nav_menu(
							array(
								'theme_location' => 'primary',
								'container' => '',
								'menu_class' => 'menu cw-nav-ul',
								'depth' => 2,
								'fallback_cb' => 'wp_page_menu',
							)
						);
					?>
				</nav>
			</div>
		</div>
	</header>

	<?php
		if(is_front_page()) {
			get_template_part('template-parts/content', 'slides');
		} else {
			get_template_part('template-parts/content', 'pageheader');
		}
	?>