<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */

get_header(); ?>

	<div class="main" role="main">
		<div class="row" style="margin-bottom: 75px;">
			<div class="s12">
				<?php
					$newsid = get_option('page_for_posts');
					if(!empty($newsid)) {
						echo '<h2 class="page-title">'.get_the_title($newsid).'</h2>';
					} else {
						echo '<h2 class="page-title">Blog</h2>';
					}

					$itn_cap = cw_options_get_option('_cwo_itn_cap');
					if($itn_cap){
						echo '<p>'.nl2br($itn_cap).'</p>';
					}
				?>
			</div>
		</div>

		<div class="row">
			<div class="s12 m8 blog-list">
				<?php
					if ( have_posts() ) {
						while ( have_posts() ) {
							the_post();
							get_template_part( 'template-parts/content', get_post_type() );
						}
					}
				?>

				<nav class="prev-next-posts row">
					<div class="prev-posts-link s6 columns">
						<?php echo get_next_posts_link( 'Older Entries', $posts->max_num_pages ); // display older posts link ?>
					</div>
					<div class="next-posts-link s6 columns">
						<?php echo get_previous_posts_link( 'Newer Entries' ); // display newer posts link ?>
					</div>
				</nav>			
			</div>

			<?php get_sidebar(); ?>
		</div>
	</div><!-- .content -->
<?php get_footer(); ?>