<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?>

	<div id="searchform_js" role="search">
		<div class="input-mother">
			<div v-on:click="reset" class="reset" v-if="input">
				<?php echo cw_get_icon_svg('undo'); ?>
			</div>

			<input class="cw-ajax-search" type="text" value="" name="s" placeholder="Search" id="s" autocomplete="off" v-on:keyup="cw_ajax_search" v-model="input" />
		</div>

		<div class="search-results">
			<div class="result cw-js-result" v-for="result in results">
				<p class="result-title"><a v-bind:href="result.url" v-html="result.title"></a></p>
			</div>

			<div class="more-links" v-if="more.url || input">
				<a v-bind:href="more.url" class="more-results" v-if="more.url" >More Results</a>
				<p class="no-results" v-if="!searching && !results.length">No Results found for: {{ input }}</p>
			</div>
		</div>
	</div>