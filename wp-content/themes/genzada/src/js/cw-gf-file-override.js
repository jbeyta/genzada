/* global jQuery: false */

(function () {
	'use strict';
	
	jQuery(document).ready(function () {
		// override gravity forms file upload appearance
		jQuery('input[type="file"]').each(function () {
			let target = jQuery(this).attr('id');
			jQuery(this).css({
				'display': 'none'
			});
			jQuery('[for=' + target + ']').addClass('button');

			jQuery(this).change(function (e) {
				// below taken from http://tympanus.net/codrops/2015/09/15/styling-customizing-file-inputs-smart-way/, may need to modify
				let fileName = '';
				if( this.files && this.files.length > 1 ) {
					fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
				} else {
					fileName = e.target.value.split( '\\' ).pop();
				}

				if( fileName ) {
					jQuery('[for=' + target + ']').html(fileName);
				}
			});
		});

	});
}());