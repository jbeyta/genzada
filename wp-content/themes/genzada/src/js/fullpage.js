/* global fullpage_api: false */

import Vue from 'vue';
import 'fullpage.js/vendors/scrolloverflow' // Optional. When using scrollOverflow:true
// import './fullpage.scrollHorizontally.min' // Optional. When using fullpage extensions
import VueFullPage from 'vue-fullpage.js'
import 'fullpage.js/dist/fullpage.min.css';

const cwPages = document.querySelector('#cw-pages-mother');

if (cwPages) {
    Vue.use(VueFullPage);

    // put the footer in the last element
    const mainEl = document.querySelector('[role="main"]');
    const footerEl = mainEl.querySelector('[role="contentinfo"]');
    if (footerEl) {
        const pageEls = cwPages.querySelectorAll('.cw-page');

        if (pageEls) {
            pageEls[pageEls.length - 1].appendChild(footerEl);
        }
    }

    new Vue({
        el: cwPages,
        data: {
            nav: [],
            showDownArrow: true,
            breakpoint: 'phone',
            loading: false,
            options: {
                licenseKey: 'A67E54D8-11824364-8FEA2C1D-2F847C4D',
                menu: '#cwpages_menu',
                anchors: [],
                scrollOverflow: true,
                paddingTop: '75px'
            }
        },
        created() {
            window.addEventListener('load', this.updateBreakpoint);
            window.addEventListener('load', this.currentSlide);
            window.addEventListener('resize', this.updateBreakpoint);
        },
        mounted: function () {
            this.createNav();
            this.updateBreakpoint();
        },
        methods: {
            currentSlide: function () {
                fullpage_api.getActiveSection();
            },
            backtotop: function () {
                const bodyEl = document.querySelector('body');
                const lastClass = 'fp-viewing-page' + (this.options.anchors.length - 1);

                if (cwHasClass(bodyEl, lastClass)) {
                    fullpage_api.moveTo(1);
                } else {
                    fullpage_api.moveSectionDown();
                }

            },
            updateBreakpoint: function () {
                this.breakpoint = window.getComputedStyle(document.querySelector('body'), ':before').getPropertyValue('content').replace(/\"/g, '');
            },
            mobileNavToggle: function () {
                const menu = document.querySelector('.scrolly-nav-list');
                const menuToggle = document.querySelector('.menu-toggle');

                if (menu) {
                    cwToggleClass(menu, 'open');
                }

                if (menuToggle) {
                    cwToggleClass(menuToggle, 'open');
                }
            },
            createNav: function () {
                const sections = cwPages.querySelectorAll('.cw-page');

                if (sections) {
                    for (let s = 0; s < sections.length; s++) {
                        const sect = sections[s];

                        let sectionTitleEl = sect.querySelector('.section-title');
                        if (!sectionTitleEl) {
                            sectionTitleEl = sect.querySelector('.page-title');
                        }

                        let sectData = {};
                        sectData.id = s;

                        if (s == 0) {
                            sectData.title = 'Home';
                        } else {
                            sectData.title = sectionTitleEl.innerHTML;
                        }

                        this.nav.push(sectData);
                        this.options.anchors.push('page' + s);
                    }
                }
            },
        }
    });
}

const cwHasClass = function (el, className) {
    if (el.classList)
        return el.classList.contains(className);
    else
        return new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className);
}

// const cwAddClass = function (el, className) {
//     if (el.classList)
//         el.classList.add(className);
//     else
//         el.className += ' ' + className;
// }

// const cwRemoveClass = function (el, className) {
//     if (el.classList)
//         el.classList.remove(className);
//     else
//         el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
// }

const cwToggleClass = function (el, className) {
    if (el.classList) {
        el.classList.toggle(className);
    } else {
        var classes = el.className.split(' ');
        var existingIndex = -1;
        for (var i = classes.length; i--;) {
            if (classes[i] === className)
                existingIndex = i;
        }

        if (existingIndex >= 0)
            classes.splice(existingIndex, 1);
        else
            classes.push(className);

        el.className = classes.join(' ');
    }
}

