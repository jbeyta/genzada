/* global jQuery: false */

(function () {
	'use strict';
	
	// jquery stuff
	jQuery(document).ready(function () {
		let $alert_mother = jQuery('.alert-cont');
		let alert_h = $alert_mother.find('.alert').outerHeight();
		let $alert_open = jQuery('.alert-open');

		function cw_alert_init() {
			if (window.breakpoint.value == 'phone' || !jQuery('body').hasClass('home')) {
				cw_closeAlert();
			} else {
				cw_openAlert();
			}
		}

		cw_alert_init();

		function cw_closeAlert() {
			$alert_mother.removeClass('showing').removeAttr('style');
			$alert_open.addClass('showing');
		}

		function cw_openAlert() {
			$alert_mother.addClass('showing');
			$alert_open.removeClass('showing');

			$alert_mother.css({
				'height': alert_h+'px'
			});
		}

		jQuery('.alert-close').click(function () {
			cw_closeAlert();
		});

		jQuery('.alert-open').click(function () {
			cw_openAlert();
		});
	});
}());