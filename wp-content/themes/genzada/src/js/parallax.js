(function () {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function (callback) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function () { callback(currTime + timeToCall); },
                timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function (id) {
            clearTimeout(id);
        };
}());

const parallaxes = document.querySelectorAll('.cwparallax');
if (parallaxes.length) {
    for (let p = 0; p < parallaxes.length; p++) {
        const prlxEl = parallaxes[p];

        // get the backround image
        // const bgImgUrl = getComputedStyle(prlxEl)['background-image'].replace('url("', '').replace('")', '');
        const bgImgUrl = getComputedStyle(prlxEl)['background-image'];

        // clear the background image
        prlxEl.style.background = 'none';

        // build new element and give it the background
        let bgEl = document.createElement("div");
        bgEl.className = 'prlx-bg';
        bgEl.style.backgroundImage = bgImgUrl;

        // stick the new element inside the original element
        prlxEl.appendChild(bgEl);

        // now lets see where it in the document
        // then measure it's distance to the top of the window
        // and start parallax motion when it comes into view
        // and stop the parallax motion when it exits view
        const prlxElPos = cwOffset(prlxEl);
        const prlxElH = prlxEl.offsetHeight;
        const prlxElTop = Math.round(prlxElPos.top);
        const prlxElBottom = Math.round(prlxElPos.top + prlxElH);
        // const prlx_win_h_diff = Math.round(windowH - prlxElH);

        const windowH = window.innerHeight;

        let last_known_scroll_position = 0;
        let ticking = false;

        // scroll event listener
        window.addEventListener('scroll', function () {
            last_known_scroll_position = window.scrollY;

            if (!ticking) {
                window.requestAnimationFrame(function () {
                    let scroll_pos = last_known_scroll_position + windowH; // scroll position at bottom of window
                    let topPos = (scroll_pos - prlxElBottom) * -1; // when we reach the top of the target element

                    // now we need to calcuate the percentage for the background position
                    // this is basically the percentage of how much window is visible from where the parallax element starts to the bottom of the window
                    let topOffset = (((topPos + windowH) / (prlxElH + windowH)) * 100);

                    // then we invert the percentage so it starts at 0 and goes to 100 to make the parallax go backwards if needed
                    // let topOffsetInverted = (topOffset - 100) * -1;

                    if (scroll_pos > prlxElTop) {
                        // let whereAmI = (topPos / prlxElH) * 100;

                        let bgpos = topOffset + '%';

                        bgEl.style.backgroundPosition = '50% ' + bgpos;
                    } else if (scroll_pos > prlxElBottom) {
                        bgEl.style.backgroundPosition = '50% 0';
                    } else {
                        bgEl.style.backgroundPosition = '50% 100%';
                    }

                    ticking = false;
                });

                ticking = true;
            }
        });
    }
}

function cwOffset(el) {
    var rect = el.getBoundingClientRect(),
        scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
        scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return {
        top: rect.top + scrollTop,
        left: rect.left + scrollLeft
    }
}
