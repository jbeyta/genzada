(function () {
	'use strict';
	
	// jquery stuff
	jQuery('.cwa-section-header').on('click', function () {
		let $this = jQuery(this);
		let $target = jQuery('#' + $this.data('target'));
		let h = $target.find('.inner').outerHeight();

		if($this.hasClass('open-header')) {
			$this.removeClass('open-header');
			$target.removeAttr('style');
		} else {
			jQuery('.open-header').removeClass('open-header');
			jQuery('.cwa-section-content').removeAttr('style');

			$this.addClass('open-header');

			$target.css({
				'height': h+10+'px',
				'padding-top': 10+'px'
			});
		}
	});
}());