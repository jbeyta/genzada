/* global wp: false, siteurl: false */

import React from 'react';

if (wp.element && wp.editor) {
	const { __ } = wp.i18n; // Import __() from wp.i18n
	const {
		registerBlockType,
		getBlockDefaultClassName
	} = wp.blocks; // Import registerBlockType() from wp.blocks\

	// const {
	// 	Toolbar,
	// 	Button,
	// 	Tooltip,
	// 	PanelBody,
	// 	PanelRow,
	// 	FormToggle,
	// } = wp.components;
	
	const {
		InspectorControls,
		BlockControls,
		MediaUpload // Thanks WP!
	} = wp.editor;

	const el = wp.element.createElement;

	const icon = el('svg',{
			xmlns: 'http://www.w3.org/2000/svg',
			viewBox: '0 0 93 93',
		},
		el('title', 'icon' ),
		el('g',{
			id: 'Layer_2',
			'data-name': 'Layer 2'
		},
			el('g',{
				id: 'Layer_1-2',
				'data-name': 'Layer 1'
			},
				el('path',{
					d: 'M30,30V93H93V30ZM80.47,61l-19,19L50.66,69.11,36,83.77V36H87V67.52Z'
				}),
				el('circle',{
					cx: '46.21',
					cy: '46.53',
					r: '5.76',
				}),
				el('polygon', {
					points: '6 87 6 6 87 6 87 13 93 13 93 0 0 0 0 93 13 93 13 87 6 87'
				}),
				el('polygon', {
					points: '21 87 21 21 87 21 87 28 93 28 93 15 15 15 15 93 28 93 28 87 21 87'
				}),
			)
		)
	);

	registerBlockType('cw-better-responsive-images-block/better-responsive-images', {
		title: 'Responsive Image',
		icon: icon,
		category: 'embed',
		supports: { // Hey WP, I want to use your alignment toolbar!
			align: true,
		},
		attributes: {
			imageData: {
				type: 'object',
				default: {},
			},
			imgW: {
				type: 'string',
				default: '',
			},
			imgH: {
				type: 'string',
				default: '',
			},
			imgWMed: {
				type: 'string',
				default: '',
			},
			imgHMed: {
				type: 'string',
				default: '',
			},
			imgWSmall: {
				type: 'string',
				default: '',
			},
			imgHSmall: {
				type: 'string',
				default: '',
			},
			crop: {
				type: 'number',
				default: 0,
			},
			loading: {
				type: 'number',
				default: 0
			},
			linkURL: {
				type: 'string',
				default: ''
			},
			targetBlank: {
				type: 'number',
				default: 0
			},
			left: {
				type: 'number',
				default: 0
			},
			centered: {
				type: 'number',
				default: 0
			},
			right: {
				type: 'number',
				default: 0
			},
			alignment: {
				type: 'string',
				default: 'none',
			}
		},

		edit: props => {
			const { className, setAttributes } = props;
			const { attributes } = props;

			const setW = (e) => {
				let imgW = e.target.value;
				setAttributes({ imgW: imgW });
			}
			const setH = (e) => {
				let imgH = e.target.value;
				setAttributes({ imgH: imgH });
			}

			const setWMed = (e) => {
				let imgW = e.target.value;
				setAttributes({ imgWMed: imgW });
			}
			const setHMed = (e) => {
				let imgH = e.target.value;
				setAttributes({ imgHMed: imgH });
			}

			const setWSmall = (e) => {
				let imgW = e.target.value;
				setAttributes({ imgWSmall: imgW });
			}
			const setHSmall = (e) => {
				let imgH = e.target.value;
				setAttributes({ imgHSmall: imgH });
			}

			const setURL = (e) => {
				let linkURL = e.target.value;
				setAttributes({ linkURL: linkURL });
			}

			const setCrop = () => {
				let cropped = document.querySelector('#cwcropimage').checked ? 1 : 0;
				setAttributes({ crop: cropped });
			}

			const setTarget = () => {
				let checked = document.querySelector('#cwtarget').checked ? 1 : 0;
				setAttributes({ targetBlank: checked });
			}

			const update = () => {
				if (attributes.imageData.id) {
					getImageHTML();
				}
			}

			const getImageHTML = (e) => {
				let img_id = '';

				setAttributes({loading: 1});

				if (e) {
					img_id = e.id;
				} else {
					img_id = attributes.imageData.id;
				}

				let url = new URL(siteurl + '/wp-json/cw/v2/img');
				url.searchParams.append('img_id', img_id);
				url.searchParams.append('crop', attributes.crop);
				url.searchParams.append('imgW', attributes.imgW);
				url.searchParams.append('imgH', attributes.imgH);
				url.searchParams.append('imgWMed', attributes.imgWMed);
				url.searchParams.append('imgHMed', attributes.imgHMed);
				url.searchParams.append('imgWSmall', attributes.imgWSmall);
				url.searchParams.append('imgHSmall', attributes.imgHSmall);

				fetch(url).then(response => response.json()).then(data => {
					// console.log(url);
					// console.log(data) // Prints result from `response.json()` in getRequest

					if (data.code != 'rest_no_route') {
						setAttributes({ imageData: data });
					}
					setAttributes({loading: 0});
				}).catch(error => {
					console.error(error)
					setAttributes({loading: 0});
				});
			}

			let loadingIndicator = <div class="loading">Loading</div>;
			if (!attributes.loading) {
				loadingIndicator = '';
			}

			let imageHTML, blockCtrlEditImg;
			let imageClasses = attributes.alignment;

			if (attributes.imageData.src) {
				blockCtrlEditImg = <MediaUpload
					onSelect={getImageHTML}
					render={({ open }) => {
						return (
							<button onClick={open}>
								<svg style={{'width': '20px', 'cursor': 'pointer', 'margin-top': '5px'}} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 248.02 248.37"><title>edit</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M185.79,0,27.4,158.39l62.23,62.23L248,62.23Zm33.59,46.32L73.72,192,56,174.3,201.7,28.64ZM185.79,12.73l13.08,13.08L53.21,171.47,40.13,158.39ZM89.63,207.89,76.54,194.81,222.21,49.14l13.08,13.09Z" /><polygon points="43.5 236.72 83.65 225.96 22.41 164.72 11.65 204.87 43.5 236.72" /><polygon points="8.07 218.26 0 248.37 30.11 240.3 8.07 218.26" /></g></g></svg>
							</button>
						);
					}}
				/>;

				if (attributes.linkURL) {
					imageHTML = el('a', {
						href: attributes.linkURL,
						target: attributes.targetBlank ? '_blank' : '',
						rel: 'noopener nofollow noreferrer'
					},
						el('img', {
							className: imageClasses,
							src: attributes.imageData.src,
							srcset: attributes.imageData.srcset,
							sizes: attributes.imageData.sizes,
							alt: attributes.imageData.alt,
						})
					)
				} else {
					imageHTML = el('img', {
						className: imageClasses,
						src: attributes.imageData.src,
						srcset: attributes.imageData.srcset,
						sizes: attributes.imageData.sizes,
						alt: attributes.imageData.alt,
					})
				}
			} else {
				blockCtrlEditImg = '';

				imageHTML = <MediaUpload
					onSelect={getImageHTML}
					render={({ open }) => {
						return (
							<button onClick={open}>Select/Upload Image</button>
						);
					}}
				/>;
			}

			return [
				<InspectorControls>
					<div className="cw-better-responsive-image-controls">
						{loadingIndicator}
						<div className="cwib-wrap-border">
							<div className="input-mother">
								<label>URL</label>
								<input type="url" value={attributes.linkURL} onChange={setURL} />
							</div>

							<div className="input-mother">
								<label>Open link in new tab</label>
								<input id="cwtarget" type="checkbox" value="1" onChange={setTarget} checked={!!attributes.targetBlank} />
							</div>
						</div>

						<div className="cwib-wrap">
							<p><b>Breakpoints:</b> Both width and height are required to create a cropped image.</p>
							<p><b>NOTE:</b> Make sure to click "Update Size/Crop" after changing values below. You may not see a change in the editor, but trust me, it works.</p>
						</div>

						<div className="cwib-wrap-border">
							<div className="input-mother">
								<h5>Small (phones)</h5>
							</div>

							<div className="input-mother">
								<label>Width</label>
								<input type="text" value={attributes.imgWSmall} onChange={setWSmall} />
							</div>

							<div className="input-mother">
								<label>Height</label>
								<input type="text" value={attributes.imgHSmall} onChange={setHSmall} />
							</div>
						</div>

						<div className="cwib-wrap-border">
							<div className="input-mother">
								<h5>Medium (tablets)</h5>
							</div>

							<div className="input-mother">
								<label>Width</label>
								<input type="text" value={attributes.imgWMed} onChange={setWMed} />
							</div>

							<div className="input-mother">
								<label>Height</label>
								<input type="text" value={attributes.imgHMed} onChange={setHMed} />
							</div>
						</div>

						<div className="cwib-wrap-border">
							<div className="input-mother">
								<h5>Large (desktop)</h5>
							</div>

							<div className="input-mother">
								<label>Width</label>
								<input type="text" value={attributes.imgW} onChange={setW} />
							</div>

							<div className="input-mother">
								<label>Height</label>
								<input type="text" value={attributes.imgH} onChange={setH} />
							</div>
						</div>

						<div className="input-mother">
							<label>Crop</label>
							<input id="cwcropimage" type="checkbox" value="1" onChange={setCrop} checked={!!attributes.crop} />
						</div>

						<div className="input-mother">
							<button onClick={update}>Update Size/Crop</button>
						</div>
					</div>
				</InspectorControls>,
                <BlockControls>
					{blockCtrlEditImg}
                </BlockControls >,
				<div className={className}>
					{imageHTML}
				</div>
			];
		},

		save: (props) => {
			const className = getBlockDefaultClassName('cw-better-responsive-images-block/better-responsive-images');
			const { attributes } = props;

			let imageClasses = attributes.alignment;

			if (attributes.linkURL) {
				return (
					el('div', {
						className: className,
					},
						el('a', {
							href: attributes.linkURL,
							target: attributes.targetBlank ? '_blank' : '',
							rel: 'noopener nofollow noreferrer'
						},
							el('img', {
								className: imageClasses,
								src: attributes.imageData.src,
								srcset: attributes.imageData.srcset,
								sizes: attributes.imageData.sizes,
								alt: attributes.imageData.alt,
							})
						)
					)
				)
			} else {
				return (
					el('div', {
						className: className,
					},
						el('img', {
							className: imageClasses,
							src: attributes.imageData.src,
							srcset: attributes.imageData.srcset,
							sizes: attributes.imageData.sizes,
							alt: attributes.imageData.alt,
						})
					)
				)
			}
		},
	});
}