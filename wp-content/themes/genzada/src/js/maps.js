/* global jQuery: false, google: false, map_options: false */

import Vue from 'vue';
import LoadScript from 'vue-plugin-load-script';

if (jQuery('.cw-map-canvas').length) {
	Vue.use(LoadScript);
	Vue.loadScript('https://maps.googleapis.com/maps/api/js?key='+map_options.maps_api_key).then(() => {
		jQuery('.cw-map-canvas').each(function () {
			let $this = jQuery(this);
			let id = $this.attr('id');	
			let address = $this.data('address');

			new Vue({
				el: '#'+id,
				data: {
					results: {},
				},
				mounted: function() {
					// this.$loadScript('https://maps.googleapis.com/maps/api/js?key='+map_options.maps_api_key).then(() => {
					// 	this.draw_map();
					// }).catch(() => {
					// 	this.draw_map();
					// 	console.log('Failed to fetch script');
					// });
					this.draw_map();
				},
				methods: {
					draw_map: function() {
						let map;
						// let bounds = new google.maps.LatLngBounds();
						let mapOptions = {
							mapTypeId: 'roadmap',
							zoom: parseInt(map_options.zoom),
							scrollwheel: map_options.scroll_zoom,
							disableDefaultUI: map_options.controls
						};

						let customMapType = new google.maps.StyledMapType([
							{
								stylers: [
									{hue: map_options.hue},
									{visibility: map_options.visibility},
									{saturation: map_options.satch},
								]
							}
							], {
							name: 'Custom Style'
						});

						let customMapTypeId = 'custom_style';

						// Display a map on the page
						map = new google.maps.Map(document.getElementById(id), mapOptions);

						let geocoder = new google.maps.Geocoder();
						
						geocoder.geocode({ 'address': address }, function(results, status) {
							if (status == google.maps.GeocoderStatus.OK) {
								let position = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());

								new google.maps.Marker({
									position: position,
									map: map,
									animation: google.maps.Animation.DROP,
								});

								map.setOptions({center:position});
							}
						});

						map.mapTypes.set(customMapTypeId, customMapType);
						map.setMapTypeId(customMapTypeId);
					}
				}
			});	
		});
	}).catch((e) => {
		console.log(e);
	});
}