/* global endpoints: false, jQuery: false */
// https://www.npmjs.com/package/vue-carousel

import Vue from 'vue';
import { Carousel, Slide } from 'vue-carousel';

// slides
const sliderEl = document.querySelector('#cw_ss');
if (sliderEl) {
	new Vue({
		el: sliderEl,
		components: {
			'carousel': Carousel,
			'slide': Slide
		},
		template: `
			<div class="cw-slideshow">
				
				<div class="loading" v-if="loading"></div>
				<div class="down-arrow" v-on:click="scrolly"></div>

				<carousel
					:perPage="1"
					:autoplay="false"
					:loop="true"
					:autoplayTimeout="5000"
					:navigationEnabled="false"
					:paginationEnabled="false"
					:mouseDrag="false"
				>
					<slide v-for="(slide, i) in slides" :key="i">
						<div class="image-mother">
							<div class="cover"></div>
							<div class="inner" v-html="slide.slide_image"></div>
						</div>

						<div class="slide-words" v-if="slide.slide_title || slide.slide_caption || slide.slide_link">
							<div class="inner">
								<h3 class="slide-title" v-if="slide.slide_title" v-html="slide.slide_title"></h3>
								<p class="slide-caption" v-if="slide.slide_caption" v-html="slide.slide_caption"></p>
								<a class="slide-button" v-if="slide.slide_link" v-bind:href="slide.slide_link">Learn More</a>
							</div>
						</div>
					</slide>
				</carousel>
			</div>
		`,
		data: {
			slides: {},
			loading: false
		},
		mounted: function () {
			this.get_slides();
		},
		updated: function () {
			this.loading = false;
		},
		methods: {
			scrolly: function () {
				const mainEl = document.querySelector('[role="main"]');

				if (mainEl) {
					const mainElPos = this.cwOffset(mainEl);

					jQuery('html, body').animate({
						scrollTop: mainElPos.top - 145
					}, 500);
				}
			},
			cwOffset: function(el) {
				var rect = el.getBoundingClientRect(),
					scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
					scrollTop = window.pageYOffset || document.documentElement.scrollTop;
				return {
					top: rect.top + scrollTop,
					left: rect.left + scrollLeft
				}
			},
			get_slides: function () {
				this.loading = true;
				let self = this;

				jQuery.ajax({
					url: endpoints.slides,
					type: 'GET',
					// beforeSend: function(xhr) {},
					success: function(data){
						// console.log('success');
						// console.log(data);
						self.slides = data;
						self.loading = false;
					},
					error: function(jqXHR, textStatus, errorThrown){
						console.log('jqXHR: ', jqXHR);
						console.log('textStatus: ', textStatus);
						console.log('errorThrown: ', errorThrown);
						console.log('status: ', jqXHR.status);
						console.log('responseText: ', jqXHR.responseText);
						self.loading = false;
					}
				});
			}
		}
	});
}