import Vue from 'vue';
import axios from 'axios'
import VueAxios from 'vue-axios'
import LoadScript from 'vue-plugin-load-script';

(function () {
	'use strict';

	let cwblocks = document.querySelectorAll('div.cw-cpt-block');
	if(cwblocks.length){
		Vue.use(VueAxios, axios);

		for (var i = 0; i < cwblocks.length; i++) {
			let type = cwblocks[i].getAttribute('data-type');
			let per_page = cwblocks[i].getAttribute('data-per_page');
			let orderby = cwblocks[i].getAttribute('data-orderby');
			let order = cwblocks[i].getAttribute('data-order');
			let cat = cwblocks[i].getAttribute('data-cat');

			let wp_classes = [];
			wp_classes = cwblocks[i].classList;
			
			let template = ``;

			if(type == 'faqs') {
				template = `
					<div v-bind:class="'faqs cw-accordion '+wp_classes">
						<div class="loader" v-if="loading"></div>
						<div class="faq child" v-for="faq in results">
							<h4 class="cwa-section-header" v-html="faq.title.rendered" v-on:click="faq_openclose($event)"></h4>
							<div class="cwa-section-content">
								<div class="inner" v-html="faq.content.rendered">
								</div>
							</div>
						</div>
					</div>
				`;
			}

			if(type == 'galleries') {
				template = `
					<div v-bind:class="'galleries row '+wp_classes">
						<div class="loader" v-if="loading"></div>
						<div class="gal child s6 m4 l3" v-for="gallery in results">
							<h3 class="gal-title"><a v-bind:href="gallery.link" v-html="gallery.title.rendered"></a></h3>
							<a v-bind:href="gallery.link" v-html="gallery.post_meta.thumbs[0]"></a>
						</div>
					</div>
				`;
			}

			if(type == 'testimonials') {
				template = `
					<div v-bind:class="'testimonials '+wp_classes">
						<div class="loader" v-if="loading"></div>
						<blockquote class="testimonial child" v-for="tstm in results">
							<p class="quote" v-if="tstm.post_meta.testimonial" v-html="tstm.post_meta.testimonial"></p>

							<div class="info"><b v-html="tstm.title.rendered"></b>
								<p class="vocation" v-if="tstm.post_meta.vocation">{{tstm.post_meta.vocation}}</p>
								<p class="location" v-if="tstm.post_meta.location">{{tstm.post_meta.location}}</p>
							</div>
						</blockquote>
					</div>
				`;
			}

			if(type == 'staff') {
				template = `
					<div v-bind:class="'staff-list '+wp_classes">
						<div class="loader" v-if="loading"></div>
						<div class="staff-content" v-for="staff in results">
							<div class="img-cont" v-if="staff.post_meta.image" v-html="staff.post_meta.image"></div>

							<div class="info" >
								<h3 class="staff-name" v-html="staff.title.rendered"></h3>
								<h5 class="staff-title" v-if="staff.post_meta.title" v-html="staff.post_meta.title"></h5>
								<p class="email" v-if="staff.post_meta.email"><a v-bind:href="'mailto:'+staff.post_meta.email">{{staff.post_meta.email}}</a></p>
								<p class="phone" v-if="staff.post_meta.phone"><a v-bind:href="'tel:'+staff.post_meta.phone_clean">{{staff.post_meta.phone}}</a></p>
							</div>
						</div>
					</div>
				`;
			}


			// console.log('loaded');
			let cwblocks_mother = new Vue({
				el: cwblocks[i],
				template: template,
				data: {
					results: [],
					wp_classes: wp_classes.value,
					loading: false,
				},
				mounted() {
					this.query();
				},
				updated() {
					if(this.wp_classes.includes('slider')) {
						this.fire_it_up();
					}
				},
				methods: {
					query() {
						this.loading = true;

						let self = this;

						let url = siteurl+'/wp-json/wp/v2/'+type;

						const params = new URLSearchParams();
						let formvals = {};
						
						if(per_page >= 1) { formvals.per_page = per_page; }
						if(order) { formvals.order = order; }
						if(orderby) { formvals.orderby = orderby; }
						if(cat) { formvals[type+'_categories'] = cat; }

						for (const [key, val] of Object.entries(formvals)) {
						   params.append(key, val);
						}

						url = url+'?'+params.toString();

						axios.get(url).then(response => {
							// console.log(response);
							if(response.data.length) {
								for (var i = 0; i < response.data.length; i++) {
									Vue.set(self.results, i, response.data[i]);
								}
							}
							self.loading = false;
						}).catch(error => {
							self.loading = false;
							console.log(error)
						});
					},
					faq_openclose(e){
						let title_el = e.target;
						let content_el = title_el.nextElementSibling;

						let className = 'open-header';

						if(title_el.classList.contains(className)) {
							title_el.classList.remove(className);
							content_el.removeAttribute('style');
						} else {
							let open_headers = document.querySelectorAll('.'+className);

							for (let i = 0; i < open_headers.length; i++) {
								open_headers[i].classList.remove(className);
								open_headers[i].nextElementSibling.removeAttribute('style');
							}

							title_el.classList.add(className);

							let h = content_el.querySelectorAll('.inner')[0].offsetHeight;

							content_el.style.height = h+'px';
						}
					},
					fire_it_up: function(){
						let self = this;

						this.slider = $('.cw-cpt-block.slider');
						let count = this.slider.find('.child').length;

						if(count > 1) {
							let cwso_options = {
								autoplay: true,
								autoplaySpeed: 5000,
								arrows: false,
								infinite: true,
								speed: 500,
								dots: true,
								centerMode: true,
								slidesToShow: 3,
								slidesToScroll: 1,
								responsive: [
									{
										breakpoint: 1024,
										settings: {
											slidesToShow: 1
										}
									}
								]
							};

							// the .not(...) needed to make sure we don't init the slider more than once
							this.slider.not('.slick-initialized').slick(cwso_options);
						} else {
							setTimeout(function(){
								self.slider.find('.slide:eq(0)').addClass('active-slide');
							}, 500);
						}
					}
					// draw_map: function() {
					// 	let map, marker;
					// 	let bounds = new google.maps.LatLngBounds();
					// 	let mapOptions = {
					// 		mapTypeId: 'roadmap',
					// 		zoom: parseInt(map_options.zoom),
					// 		scrollwheel: map_options.scroll_zoom,
					// 		disableDefaultUI: map_options.controls
					// 	};
								 
					// 	let customMapType = new google.maps.StyledMapType([
					// 		{
					// 			stylers: [
					// 				{hue: map_options.hue},
					// 				{visibility: map_options.visibility},
					// 				{saturation: map_options.satch},
					// 			]
					// 		}
					// 		], {
					// 		name: 'Custom Style'
					// 	});

					// 	let customMapTypeId = 'custom_style';

					// 	// Display a map on the page
					// 	map = new google.maps.Map(document.getElementById(id), mapOptions);

					// 	let geocoder = new google.maps.Geocoder();
						
					// 	geocoder.geocode({ 'address': address }, function(results, status) {
					// 		if (status == google.maps.GeocoderStatus.OK) {
					// 			let position = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());

					// 			marker = new google.maps.Marker({
					// 				position: position,
					// 				map: map,
					// 				animation: google.maps.Animation.DROP,
					// 			});

					// 			map.setOptions({center:position});
					// 		}
					// 	});

					// 	map.mapTypes.set(customMapTypeId, customMapType);
					// 	map.setMapTypeId(customMapTypeId);
					// }
				}
			});
		}
	}
		
	
}());