// staff
const staffMothers = document.querySelectorAll('.staff-from-sc');

if (staffMothers.length) {
    for (let s = 0; s < staffMothers.length; s++) {
        const staffMother = staffMothers[s];
        const staffBlocks = staffMother.querySelectorAll('.staff-content');

        if(staffBlocks.length) {
            for (let i = 0; i < staffBlocks.length; i++) {
                const staff = staffBlocks[i];

                // get the readmore button, this will expand to show the content
                const readMore = staff.querySelectorAll('.readmore');

                // get all children of parent
                const siblings = Array.prototype.slice.call(staff.parentNode.children);

                // remove this item, leave siblings only
                for (let s = siblings.length; s--;) {
                    if (siblings[s] === staff) {
                        siblings.splice(s, 1);
                        break;
                    }
                }

                // get the block containing the content and measure height.
                const bioBlock = staff.querySelector('.bio');
                const inner = bioBlock.querySelector('.inner');

                // the team category gets treated differently
                let isTeam = false;
                if (cwHasClass(bioBlock, 'isteam')) {
                    isTeam = true;
                }

                let introH = 0;

                if(isTeam) {
                    let intro = inner.querySelector('.intro');

                    if (!intro) {
                        intro = inner.firstElementChild;
                    }

                    introH = intro.offsetHeight;
                }

                // set the initial height
                bioBlock.style.height = introH + 'px';

                for (let rm = 0; rm < readMore.length; rm++) {
                    const readMoreBtn = readMore[rm];
                    // on click, set expanded height, hide/show siblings
                    readMoreBtn.addEventListener('click', () => {

                        const classOpen = 'expanded';
                        const classHide = 'hidden';

                        const expanded = staffMother.querySelectorAll('.' + classOpen);

                        if (expanded.length) {
                            cwRemoveClass(staffMother, classOpen);
                        } else {
                            cwAddClass(staffMother, classOpen);
                        }

                        if (cwHasClass(staff, classOpen)) {
                            // currently open, close it up
                            cwRemoveClass(staff, classOpen);

                            bioBlock.style.height = introH + 'px';

                            for (let n = 0; n < siblings.length; n++) {
                                const sib = siblings[n];
                                cwRemoveClass(sib, classHide);
                            }

                            readMoreBtn.innerHTML = 'Read More';
                        } else {
                            // currently closed, open it
                            cwAddClass(staff, classOpen);

                            for (let m = 0; m < siblings.length; m++) {
                                const sib = siblings[m];
                                cwAddClass(sib, classHide);
                            }

                            setTimeout(() => {
                                const innerH = inner.offsetHeight;
                                bioBlock.style.height = innerH + 'px';

                                readMoreBtn.innerHTML = 'Read Less';
                            }, 50);
                        }
                    });
                }
            }
        }
    }
}

// http://youmightnotneedjquery.com/
function cwHasClass(el, className) {
    if (el.classList) {
        return el.classList.contains(className);
    } else {
        return new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className);
    }
}

function cwAddClass(el, className) {
    if (el.classList)
        el.classList.add(className);
    else
        el.className += ' ' + className;
}

function cwRemoveClass(el, className) {
    if (el.classList)
        el.classList.remove(className);
    else
        el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
}