<?php
/**
 * Template Name: Research Template
 * Template Post Type: page
 * Description: Custom page template.
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>
	<div id="main-content" class="research" role="main">
		<div class="row">
			<div class="s12">
				<?php if (have_posts()) : while (have_posts()) : the_post();
					echo '<h2 class="page-title">'.get_the_title().'</h2>';
					the_content();
				endwhile; endif; ?>
				<hr>		
				<?php 
					$post_type = 'research';
					$post_args = array(
						'post_type' => $post_type,
						'posts_per_page' => -1,
						'orderby' => 'date',
						'order' => 'DESC'
					);

					$posts = new WP_Query($post_args);
					if($posts->have_posts()){
						get_template_part('template-parts/content', 'research');
					} else {
						echo '<p>No '.$post_type.' yet. Check back soon</p>';
					}

					wp_reset_query();
				?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>