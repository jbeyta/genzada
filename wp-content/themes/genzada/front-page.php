<?php
/**
 * Template Name: Home Page Template
 * Template Post Type: page
 * Description: Custom home page template.
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>
	
	<div id="main-content" role="main">
		<div class="row">
			<div class="s12">
				<h2 class="visually-hidden"><?php the_title(); ?></h2>
				
				<?php
					while(have_posts()) {
						the_post();
						the_content();
					}
				?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>