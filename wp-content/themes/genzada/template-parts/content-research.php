<?php
/**
 * @package WordPress
 * @subpackage CW 
 * @since CW 1.0
 */

    $terms_links = '';
    $term_tax = 'research_categories';
    if(taxonomy_exists($term_tax)) {
        $terms = wp_get_object_terms( $post->ID, $term_tax );
        if(!empty($terms)) {
            $terms_links .= '<p class="categories">';
            foreach ($terms as $term) {
                $terms_links .= '<a class="term-link" href="'.get_term_link($term->term_id, $term_tax).'">'.$term->name.'</a>';
            }
            $terms_links .= '</p>';
        } 
    }

    $tags_links = '';
    $tag_tax = 'research_tags';
    if(taxonomy_exists($tag_tax)) {
        $tags = wp_get_object_terms( $post->ID, $tag_tax );
        if(!empty($tags)) {
            $tags_links .= '<p class="tags">';
            foreach ($tags as $tag) {
                $tags_links .= '<a class="term-link" href="'.get_term_link($tag->term_id, $tag_tax).'">'.$tag->name.'</a>';
            }
            $tags_links .= '</p>';
        }
    }

    $weblink = get_post_meta($post->ID, '_cwmb_weblink', true);
    $target = 'target="_blank" rel="noopener nofollow"';

    if(!$weblink) {
        $weblink = get_the_permalink($post->ID);
        $target = '';
    }
?>

    <?php if(is_singular('research')) { ?>
        <article id="post-<?php the_ID(); ?>" class="research-item" style="margin-bottom: 75px;">
            <?php
                echo '<h2 class="page-title">'.get_the_title().'</h2>';
                echo '<div class="research-meta">';
                    echo '<p class="date">'.get_the_date().'</p>';
                    echo $tags_links;
                    echo $terms_links;
                echo '</div>';
                the_content();

                echo '<a href="'.$weblink.'" class="button" '.$target.'>Read More</a>';
            ?>
        </article>
    <?php } else { ?>
        <article id="post-<?php the_ID(); ?>" class="post-list-item">
            <?php
                echo '<div class="info">';
                    echo '<div class="top">';
                        echo '<p class="date">'.get_the_date().'</p>';
                        echo $tags_links;
                        echo $terms_links;
                    echo '</div>';

                    echo '<h3 class="article-title"><a href="'.get_the_permalink().'">'.get_the_title().'</a></h3>';
                echo '</div>';

                echo '<a href="'.$weblink.'" class="button" '.$target.'>Read More</a>';
            ?>
        </article>
    <?php } ?>