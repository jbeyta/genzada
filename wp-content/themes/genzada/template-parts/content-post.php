<?php
/**
 * @package WordPress
 * @subpackage CW 
 * @since CW 1.0
 */

$weblink = get_post_meta($post->ID, '_cwmb_weblink', true);
$target = 'target="_blank" rel="noopener nofollow"';

if(get_post_type() != 'post') {
    $weblink = get_the_permalink($post->ID);
    $target = '';
}

if(is_single()) {
    echo '<h2 class="page-title">'.get_the_title().'</h3>';
    // the_post_thumbnail();
    echo '<p class="date">'.get_the_date().'</p>';
    $terms = wp_get_object_terms( $post->ID, 'category' );
    if(!empty($terms)) {
        echo '<p class="categories">';
        foreach ($terms as $term) {
            echo '<a class="term-link" href="'.get_term_link($term->term_id, 'category').'">'.$term->name.'</a>';
        }
        echo '</p>';
    }
    echo '<hr>';
    the_content();
} else {
    if(is_home()) {
        echo '<article class="post">';
            echo '<h3 class="article-title">'.get_the_title().'</h3>';

            the_post_thumbnail();

            echo '<div class="inner">';
                echo '<p class="date">'.get_the_date().'</p>';
                echo '<p class="excerpt">'.get_the_excerpt().'</p>';

                if($weblink) {
                    echo '<a href="'.$weblink.'" '.$target.' class="button">Read More</a>';
                }
            echo '</div>';
        echo '</article>';
    } else {
        echo '<article class="post-list-item">';
            echo '<div class="info">';
                echo '<div class="top">';
                    echo '<p class="date">'.get_the_date().'</p>';
                    $terms = wp_get_object_terms( $post->ID, 'category' );
                    if(!empty($terms)) {
                        echo '<p class="categories">';
                        foreach ($terms as $term) {
                            echo '<a class="term-link" href="'.get_term_link($term->term_id, 'category').'">'.$term->name.'</a>';
                        }
                        echo '</p>';
                    }
                echo '</div>';
                echo '<h3 class="article-title">';
                    if($weblink) { echo '<a href="'.$weblink.'" '.$target.'>'; }
                        echo get_the_title();
                    if($weblink) { echo '</a>'; }
                echo '</h3>';
            echo '</div>';

            if($weblink) {
                echo '<a href="'.$weblink.'" '.$target.' class="button">Read More</a>';
            }
        echo '</article>';
    }
}
	