<?php
    $pageid = $post->ID;

    if(is_home()) {
        $pageid = get_option('page_for_posts');
    }

    if(is_search()) {
        $pageid = '';
    }

    if(has_post_thumbnail($pageid) && !empty($pageid)) {
        echo '<div class="page-header">';
            echo get_the_post_thumbnail($pageid, 'large');
        echo '</div>';
    } else {
        echo '<div class="page-header noimg"></div>';
    }