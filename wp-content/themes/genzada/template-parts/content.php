<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( is_single() ) {
			echo '<h2 class="page-title">'.get_the_title().'</h2>';
			the_post_thumbnail();
		} else {
			echo '<h3 class="entry-title">';
				echo '<a href="'.get_the_permalink().'" title="Permalink to '.the_title_attribute( 'echo=0' ).'" rel="bookmark">'.get_the_title().'</a>';
			echo '</h3>';
		} // is_single()

		if(get_post_type() == 'post') {
			echo '<p class="date">'.get_the_date('M j, Y').'</p>';
		}

		if ( is_single() ) {
			echo '<div class="entry-content">';
				the_content();
			echo '</div><!-- .entry-content -->';

			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}

		} else {
			echo '<div class="entry-excerpt">';
				the_excerpt( __( 'Continue reading <span class="meta-nav">&rarr;</span>' ) );
				wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:' ), 'after' => '</div>' ) );
			echo '</div><!-- .entry-content -->';
		} ?>
	</article><!-- #post -->