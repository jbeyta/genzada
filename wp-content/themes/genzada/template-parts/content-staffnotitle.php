<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
	// $title = get_post_meta($post->ID, '_cwmb_staff_title', true);
	$image_id = get_post_meta($post->ID, '_cwmb_staff_image_id', true);
	$email = get_post_meta($post->ID, '_cwmb_staff_email', true);
	$phone = get_post_meta($post->ID, '_cwmb_staff_phone', true);
	$bio = get_the_content($post->ID);

	$terms = wp_get_object_terms($post->ID, 'staff_categories');
	$isTeam = false;
	if(!empty($terms)) {
		foreach ($terms as $term) {
			if($term->slug == 'team') {
				$isTeam = true;
			}
		}
	}

	if(is_singular('staff')) {
		echo '<div class="staff-content row">';
			if(!empty($image_id)) {
				echo '<div class="m4 l3 img-cont">';
					cw_img($image_id);
				echo '</div>';

				echo '<div class="m8 l9">';
			} else {
				echo '<div class="m12">';
			}

				echo '<h2 class="staff-name page-title">'.get_the_title().'</h2>';

				// if(!empty($title)) {
				// 	echo '<h5 class="staff-title">'.$title.'</h5>';
				// }

				if(!empty($email)) {
					echo '<p class="email"><a href="mailto:'.$email.'">'.$email.'</a></p>';
				}

				if(!empty($phone)) {
					echo '<p class="phone"><a href="tel:'.$phone.'">'.$phone.'</a></p>';
				}

				if(!empty($bio)) {
					echo '<div class="bio"><div class="inner">';
						echo apply_filters('the_content', $bio);
					echo '</div></div>';
				}
			echo '</div>';
		echo '</div>';
	} else {
		echo '<div class="staff-content notitle">';
			if($isTeam) {
				echo '<h4 class="staff-name"><a href="'.get_the_permalink().'">'.get_the_title().'</a></h4>';
			} else {
				echo '<h4 class="staff-name">'.get_the_title().'</h4>';
			}
			
			// if(!empty($title)) {
			// 	echo '<h6 class="staff-title">'.$title.'</h6>';
			// }

			if(!empty($bio)) {
				$class = 'bio';
				if($isTeam) {
					$class .= ' isteam';
				}
				echo '<div class="'.$class.'"><div class="inner">';
					echo apply_filters('the_content', $bio);
				echo '</div></div>';
			}

			if($isTeam) {
				echo '<span class="readmore">Read More</span>';
			} else {
				echo '<span class="readmore low">Read More</span>';
			}
		echo '</div>';
	}